<!doctype html>
<html>
<head>
<link href="img/symbol.png" type="image/x-icon" rel="icon"/>
<link rel="stylesheet" href="css/reset.css"/>
<link rel="stylesheet" href="css/style.css"/>
<link rel="stylesheet" href="css/caption.css"/>
<meta charset="utf-8">
<link href='http://fonts.googleapis.com/css?family=Raleway:400,200,700' rel='stylesheet' type='text/css'>
<script src="js/script.js"></script>
<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>


<title>Katelyn Hare - Web Developer</title>
</head>

<body>
	<div class="home" id="home">
  
    	<img id="logo" src="img/logo.png" alt="Katelyn's Logo"/>
        </div>
        <div id="bgnav">
        <nav class="home">
        	<li><a href="index.html">Home</a></li>
            <li><a href="portfolio.html">Portfolio</a></li>
           
            <li><a href="KatelynHareResume.pdf" target="_blank">Resume</a></li>
            <li><a href="about.html">About</a></li>
            <li><a href="contact.html">Contact</a></li>
        </nav>
        </div>
<div class="home">
	
        	
       <div id="contacts"> 	
       <h2>Contact</h2>
    	<article>
        <p>Email me at <a href="mailto:katelyn@katelynhare.com">katelyn@katelynhare.com</a></p>  
<br>
     
         <?php
                if (isset($_REQUEST['email']))
        
                {
        
                    $name = $_REQUEST['name'] ;
                    $phone = $_REQUEST['phone'] ;
                     $email = $_REQUEST['email'] ;
                     $subject = $_REQUEST['subject'] ;
                     $message = $_REQUEST['message']."     Name: ".$name ."     Phone Number: ".$phone ."     Email: ".$email." " ;
                     mail("katelyn@katelynhare.com", $subject, $message, "From:" . $email);
                     echo "<h3>Thank you for contacting me</h3> <p>I will get back to you as soon as I can!</p>";
                }
                else
        
                {
                    echo "<form method='post' action='contact.php'>
                    
                        <p>Name:<br><input name='name' type='text' required></p>
                        <p>Phone Number:<br><input name='phone' placeholder='(xxx)xxx-xxxx' type='text'></p>
                        <p>Email Address:<br><input name='email' type='email' required></p>
                        <p>Subject:<br><input name='subject' type='text'></p>
                        <p>Message:</p>
                          <textarea name='message' rows='10' cols='40'></textarea>
                          <br>
                          <input type='submit'>
                          </form>";
                }
                
            ?>
       
     </article>
        	</div>
        	
        	</div>
   

        
	</div>
	<footer>
          <h6>&copy; Katelyn Hare 2014</h6>
        </footer>
    
</body>
</html>
